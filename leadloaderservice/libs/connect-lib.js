import { config, Connect } from 'aws-sdk';

config.update({ region: 'eu-central-1' });
let connect = new Connect();
/**
* Start Contact using with contant number in E16 format.
*/
export function startCall(params) {
    return connect.startOutboundVoiceContact(params).promise();
}
  /**
   * Stop Contact using contactId as a parameter.
   */
export function endCall(params){
    return connect.stopContact(params).promise();
}



import uuid from "uuid";
import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";

export async function main(event, context) {

  console.log("event:"+JSON.stringify(event));

  var mydate = Date.parse(event.Details.Parameters.Date);
  const params = {
    TableName: "calendarEvents",
    Item: {
      userID: event.Details.Parameters.cognitoIdentityId,
      eventid: uuid.v1(),
      email: event.Details.Parameters.email,
      fullname: event.Details.Parameters.name,
      number: event.Details.Parameters.number,
      title: "Call Again",
      start: mydate,
      end: mydate,
      createdAt: Date.now()
    }
  };
  try {
    await dynamoDbLib.call("put", params);
    return success(params.Item);
  } catch (e) {
    console.log(e);
    return failure({ status: false ,error:e.message});
  }
}
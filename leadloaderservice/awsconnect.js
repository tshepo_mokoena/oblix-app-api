import { config } from 'aws-sdk';
import *  as connect from './libs/connect-lib';
import { success, failure } from "./libs/response-lib";

config.update({ region: 'eu-central-1' });

export async function main(event, context) {
    const data = JSON.parse(event.body);
    const customerName = data.name;
    const customerPhoneNumber = data.number;
    const customerEmail  = data.email;

    let params = {
        "InstanceId" : '8ba7f13c-dc92-440f-bf0b-80d50e8fcc8e',
        "ContactFlowId" : '52ffbe47-f513-4cf5-8015-379f976acbbc',
        "SourcePhoneNumber" : '+448081649459',
        "DestinationPhoneNumber" : customerPhoneNumber,
        "Attributes" : {
            'name' : customerName,
            'number':customerPhoneNumber,
            'email':customerEmail,
            'userId':event.requestContext.identity.cognitoIdentityId
        }
    };
    try {
        const result = await connect.startCall(params);
        return success(result);
    } catch (e) {
      return failure({ status: false , message: e.message});
    }
}


export async function endcall(event , content){
    const data = JSON.parse(event);
    const contanctId = data.contanctId;
    try {
        const result = await connect.endCall(contanctId);
        return success(result);
    } catch (error) {
        return failure({ status: false , message: error.message});
    }
}
import * as calenderLib from "./libs/calender-lib";
import { success } from "./libs/response-lib";

export async function listCalenderEvents(event, context) {
    calenderLib.listcalenderevents();
    return success(event);
}
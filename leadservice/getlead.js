import * as dynamoDbLib from "../../oblixbackend/libs/dynamodb-lib";
import { success, failure } from "../../oblixbackend/libs/response-lib";

export async function main(event, context) {
  const params = {
    TableName: "Leads",
    // 'Key' defines the partition key and sort key of the item to be retrieved
    // - 'userId': Identity Pool identity id of the authenticated user
    // - 'leadId': path parameter
    Key: {
      userId: event.requestContext.identity.cognitoIdentityId,
      leadId: event.pathParameters.id
    }
  };

  try {
    const result = await dynamoDbLib.call("get", params);
    if (result.Item) {
      // Return the retrieved item
      return success(result.Item);
    } else {
      return failure({ status: false, error: "Item not found." });
    }
  } catch (e) {
    return failure({ status: false, error :e.message });
  }
}
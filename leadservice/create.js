import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";
import uuid from "uuid";

export async function main (event , context,callback){
    console.log("event:"+JSON.stringify(event));
    const params = {
        TableName: "Leads",
        // 'Item' contains the attributes of the item to be created
        // - 'userId': user identities are federated through the
        //             Cognito Identity Pool, we will use the identity id
        //             as the user id of the authenticated user
        // - 'callId': the original call id
        // - 'car': the model of the vehicle
        // - 'year': the year  of the vehicle
        // - 'insurance': the insurance of the vehicle
        // - 'createdAt': current Unix timestamp
        Item: {
          userId:event.Details.Parameters.cognitoIdentityId,
          leadId: uuid.v1(),
          contactId:event.ContactId,
          callId: event.Details.Parameters.callId,
          car: event.Details.Parameters.car,
          year: event.Details.Parameters.year,
          insurance: event.Details.Parameters.insurance,
          createdAt: Date.now()
        }
    };
    try {
        await dynamoDbLib.call("put",params);
        success(params.Item);
    } catch (error) {
        console.log(error.message);
        return failure({ status: false ,error:error.message});
    }
}